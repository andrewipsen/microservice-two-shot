from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, null=True, blank=True)
    closet_name = models.CharField(max_length=200, null=True, blank=True)

class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.PROTECT,
        null=True,
    )

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"pk": self.pk})
