import React, { useEffect, useState } from 'react';

function HatForm() {
    const [styleName, setStyleName] = useState('');
    const [fabric, setFabric] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

    function handleStyleNameChange(event) {
        const value = event.target.value;
        setStyleName(value);
        }
    function handleFabricChange(event) {
        const value = event.target.value;
        setFabric(value);
        }
    function handleColorChange(event) {
        const value = event.target.value;
        setColor(value);
        }
    function handlePictureUrlChange(event) {
        const value = event.target.value;
        setPictureUrl(value);
        }
    function handleLocationChange(event) {
        const value = event.target.value;
        setLocation(value);
        }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {

            style_name: styleName,
            fabric: fabric,
            color: color,
            picture_url: pictureUrl,
            location: location
            };
//this tells react to post the data to the hats port
        const hatsUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
        }
    }
        const response = await fetch(hatsUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newHat = await response.json();
            console.log(newHat);

            setStyleName('')
            setFabric('')
            setColor('')
            setPictureUrl('')
            setLocation('')
    }
}

//this is for populating the location dropdown menu
    const fetchLocations = async() => {
            const locationUrl = 'http://localhost:8100/api/locations/';
            const response = await fetch(locationUrl);

            if (response.ok) {
                const data = await response.json();
                setLocations(data.locations)
            }
        }
    useEffect (() => {
        fetchLocations();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                    <input onChange={handleStyleNameChange} value={styleName} placeholder="Style name" required type="text" id="style_name" className="form-control" />
                    <label htmlFor="style_name">Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleFabricChange} value={fabric} placeholder="fabric" required type="text" id="fabric" className="form-control" />
                    <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handleColorChange} value={color} placeholder="color" type="text" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                    <input onChange={handlePictureUrlChange} value={pictureUrl} placeholder="pictureUrl" required type="text" id="pictureUrl" className="form-control" />
                    <label htmlFor="picture_url">Picture Url</label>
                    </div>
                    <div className="mb-3">
                    <select onChange={handleLocationChange} value={location} required className="form-select" id="location">
                        <option value="">Choose a Location</option>
                        {locations.map(location => {
                        return (
                            <option key={location.id} value={location.id}>
                                {location.closet_name}
                            </option>
                        )
                        })}
                    </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
        </div>

    )
}
export default HatForm;
