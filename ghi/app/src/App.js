import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatList from './HatList';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import HatForm from './HatForm';

function App(props) {

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="hats">
            <Route path="" element={<HatList/>} />
            <Route path="new" element={<HatForm />} />
          </Route>
          <Route path="Shoes" >
            <Route index element={< ShoeList shoes={props.shoes}/>} />
            <Route path="new" element={< ShoeForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;

{/* <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route index element={< HatList hats={props.hats} /> } />
            {/* <Route path="new" element={<HatForm/>} /> */}
          // </Route > */}
