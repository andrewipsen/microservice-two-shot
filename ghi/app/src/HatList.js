import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

async function HatDelete(hat) {
    const hatDeleteUrl = `http://localhost:8090/api/hats/${hat.id}`
    const fetchConfig = {
        method: "delete"
    }
    const response = await fetch(hatDeleteUrl, fetchConfig);
    if (response.ok) {
        const data = await response.json()
        window.location.reload();

}
}

function HatList() {

    const [hats, setHats] = useState ([]);

    const fetchData = async () => {
        const url = "http://localhost:8090/api/hats";

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats)
            console.log(data)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <>
        <div className="d-grid gap-2 d-sm-flex mt-2 mb-2">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat</Link>
            </div>
        <table className="table table-striped">
        <thead>
            <tr>
                <th>Hat</th>
                <th>Fabric</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Closet</th>
            </tr>
            </thead>
            <tbody>
            {hats.map((hat, index) => {
                return (
                <tr key={hat.style_name + index }>
                    <td>{ hat.style_name }</td>
                    <td>{ hat.fabric }</td>
                    <td>{ hat.color }</td>
                    <td><img src={hat.picture_url} alt={hat.style_name} className="img-thumbnail" height="200" width="200"/></td>
                    <td>{hat.location.closet_name}</td>
                    <td>
                        <button onClick={() => HatDelete(hat)}>Delete</button>
                    </td>
                </tr>
                );
            })}
            </tbody>
        </table>
        </>
        // <p>this is the HatList</p>
    )
}
export default HatList;
