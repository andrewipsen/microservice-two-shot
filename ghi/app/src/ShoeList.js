import { Link } from 'react-router-dom';
import { React, useState, useEffect } from 'react';

async function ShoeDelete(shoe) {
    const shoeDeleteUrl = `http://localhost:8080/api/shoes/${shoe.id}`
    const fetchConfig = {
        method: "delete"
    }

    const response = await fetch(shoeDeleteUrl, fetchConfig);
    if (response.ok) {
        const data = await response.json()
        window.location.reload();
    }

}




function ShoeList(props) {

    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {
        const url = 'http://localhost:8080/api/shoes/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div>
            <br />
            <div className ='d-grid gap-5 d-sm-flex justify-content-sm-center'>
                <Link to='/shoes/new' className= 'btn btn-primary btn-lg px-4 gap-3'>Create a Shoe</Link>
            </div>
            <br />
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Name</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.id}>
                                <td>{ shoe.manufacturer }</td>
                                <td>{ shoe.name }</td>
                                <td>{ shoe.color }</td>
                                <td><img src={ shoe.picture_url } width="200" height="200" /></td>
                                <td>{ shoe.bin }</td>
                                <td><button onClick={() => ShoeDelete(shoe)}>Delete</button></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}


export default ShoeList;
