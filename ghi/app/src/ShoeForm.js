import { React, useEffect, useState } from 'react';

function ShoeForm() {



    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('')
    const [picture_url, setPicture] = useState('');
    const [bin, setBin] = useState('');
    const [bins, setBins] = useState([]);


    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }


    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }


    const handleColor = (event) => {
        const value = event.target.value;
        setColor(value);
    }


    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }


    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }



    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.manufacturer = manufacturer;
        data.name = name;
        data.picture_url = picture_url;
        data.color = color;
        data.bin = bin;
        console.log(data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(shoeUrl, fetchConfig);
        console.log(response)
        if (response.ok) {
            const newBin = await response.json();
            console.log(newBin);

            setManufacturer('');
            setName('');
            setColor('');
            setPicture('');
            setBin('');
        }
    }




    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);



    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} id="create-shoe-form">
                <div className="form-floating mb-3">
                <input onChange={handleManufacturerChange} value={manufacturer} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Model name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleColor} value={color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handlePictureChange} value={picture_url} placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture</label>
                </div>
                <div className="mb-3">
                <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                    <option value="">Choose a bin</option>
                    {bins.map(bin => {
                        return (
                            <option key={bin.id} value={bin.id}>
                                {bin.closet_name}
                            </option>
                        );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    );
};


export default ShoeForm;
