# Wardrobify

Team:

* Aurash Zandieh - Shoe Microservice
* Andrew Ipsen - Hats microservice

## Design

## Shoes microservice

I'm going to make the best shoes microservice this cohort has ever seen.

## Hats microservice

In order to access the data in wardrobe's Location model, I had to first create a Location Value Object model, which would copy the data from the real model, and through polling update the info every 60 seconds.

After, that, I could now tie my Hat model to closet locations through foreignkey methods, and ultimately populate dropdown menus when creating new hats.
